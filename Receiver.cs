﻿using Amqp;
using Amqp.Framing;
using Amqp.Sasl;
using Amqp.Types;
using log4net;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net.Security;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;
using System.Threading;

namespace tv.sis.connect
{
    /// <summary>
    /// Example SIS Connect client using AMQP 
    /// Normally, this code should not be changed - just instantiate custom RaceProcessor(s) - see RaceProcessor.cs
    /// AMQP .NET API v1 is rather low-level hence the amount of code. It doesn't support e.g. client-side failover or keepalive.
    /// v2 may be better but a refactor would be needed. Apache QPID Messaging API is an alternative but needs to be built from source for Windows. 
    /// </summary>
    public class AmqpClient
    {
        static readonly ILog _log = LogManager.GetLogger(typeof(AmqpClient));

        ManualResetEvent _quitEvent;
        ClientConfig _config;
        Connection _connection;
        List _receivers = new List();

        /// <summary>
        /// Starts a receiver in a background thread 
        /// </summary>
        /// <param name="quitEvent">This event will be triggered on failure</param>
        public async void Start(ManualResetEvent quitEvent)
        {
            this._quitEvent = quitEvent;
            try
            {
                _config = new ClientConfig();
                ConnectionFactory connectionFactory = GetConnectionFactory();
                _log.InfoFormat("Attempting connection to {0}", _config.host);
                _connection = await connectionFactory.CreateAsync(new Address(_config.host, _config.port, _config.userName, _config.password, "/", _config.scheme));

                foreach (IRaceProcessor processor in ProcessorConfig.processors) { 
                    _log.InfoFormat("Creating receiver for {0}", processor.GetType());
                    Receiver receiver = new Receiver();
                    receiver.Start(_config, new Session(_connection), OnClosed, processor);
                    _receivers.Add(receiver);
                }
            }
            catch (Exception e)
            {
                Util.LogException(_log, "Couldn't start connection/session/receiver", e);
                if (!quitEvent.WaitOne(0)) quitEvent.Set();
                if (Environment.ExitCode == 0) Environment.ExitCode = 1;
            }
        }

        public void Stop()
        {
            foreach (Receiver receiver in _receivers) {
                _log.DebugFormat("Stopping {0}", receiver);
                receiver.Stop();
            }
            if (_connection != null) _connection.Close();
        }

        ConnectionFactory GetConnectionFactory()
        {
            _log.Debug("Configuring Connection Factory");
            ConnectionFactory factory = new ConnectionFactory();

			// Scheme for AcgiveMQ is always AMQPS
            factory.SSL.Protocols = SslProtocols.Tls12;
            factory.SSL.RemoteCertificateValidationCallback = ValidateServerCertificate;
            factory.SSL.CheckCertificateRevocation = false;
			factory.SASL.Profile = SaslProfile.External;
            factory.AMQP.MaxFrameSize = 256 * 1024;
            factory.AMQP.ContainerId = "sisconnect-" + _config.queueName;

            if (_config.traceEnabled)
            {
                Trace.TraceLevel = TraceLevel.Frame;
                Trace.TraceListener = (f, a) => _log.DebugFormat(f, a);
            }

            return factory;
        }

        bool ValidateServerCertificate(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            if (sslPolicyErrors != SslPolicyErrors.None)
            {
                _log.ErrorFormat("Remote certificate validation error: {0}", sslPolicyErrors);
                Environment.ExitCode = 3;
                return false;
            }
            return true;
        }

        void OnClosed(AmqpObject sender, Error error)
        {
            if (error != null && !error.Description.Contains("There is no credit"))
            {
                _log.ErrorFormat("{0} closing down due to fatal error: {1}", sender, error.Description);
                Environment.ExitCode = 2;
            }
            if (!_quitEvent.WaitOne(0)) _quitEvent.Set();
        }

    }

    /// <summary>
    /// Config params loaded from App.config
    /// </summary>
    class ClientConfig
    {
		public string scheme { get; internal set; }
        public string host { get; internal set; }
		public int port { get; internal set; }
		public string userName { get; internal set; }
		public string password { get; internal set; }
		public string queueName { get; internal set; }
		public bool traceEnabled { get; internal set; }

        public ClientConfig()
        {
			scheme = ReadParam("sisBrokerScheme");
            host = ReadParam("sisBrokerHost");
			port = Convert.ToInt32(ReadParam("sisBrokerPort"));
			userName = ReadParam("sisUserName");
			password = ReadParam("sisPassword");
            queueName = ReadParam("sisQueueName");
            traceEnabled = ReadParam("traceEnabled").ToLower().Equals("true");
        }

        string ReadParam(string name)
        {
            string value = ConfigurationManager.AppSettings[name];
            if (value == null) throw new ArgumentNullException("Configuration property '" + name + "' is not set");
            return value;
        }

    }

    /// <summary>
    /// Class to start an AMQP .NET Lite ReceiverLink and listen for messages, passing them to an IRaceProcessor implementation
    /// </summary>
    class Receiver
    {
        static readonly ILog _log = LogManager.GetLogger(typeof(Receiver));
        static readonly ILog _deadLog = LogManager.GetLogger("tv.sis.connect.DeadLetterLog");

        ReceiverLink _receiverLink;
        IRaceProcessor _processor;
        Session _session;
        ClosedCallback _closedCallback;
        ClientConfig _config;

        /// <summary>
        /// Create and start ReceiverLink
        /// </summary>
        public void Start(ClientConfig config, Session session, ClosedCallback onClosed, IRaceProcessor processor)
        {
            NDC.Pop(); NDC.Push(processor.GetType().Name);
            this._config = config;
            this._processor = processor;
            this._session = session;

            // Set up optional message selector if the processor requires it
            Map filters = new Map();
            if (processor.GetFilter() != null)
            {
                filters.Add(
                    new Symbol("apache.org:selector-filter:string"),
                    new DescribedValue(new Symbol("apache.org:selector-filter:string"), processor.GetFilter()));
            }
            // then ReceiverLink
            _log.InfoFormat("Configuring receiver link for {0} and filters {1}", config.queueName, filters);
            _receiverLink = new ReceiverLink(
                session,
                "receiver-" + processor.GetType().Name,
                new Attach()
                {
                    SndSettleMode = SenderSettleMode.Unsettled, // Let us determine settlement (accept/release)
                    RcvSettleMode = ReceiverSettleMode.Second, // "At least once" delivery - if the server is not certain, will resend
                    Source = new Source()
                    {
                        Address = config.queueName,
                        DefaultOutcome = new Released(), // Any unprocessed message will be redelivered after downtime
                        FilterSet = filters
                    }
                },
                OnAttached
            );
            _closedCallback = onClosed;
            _receiverLink.Closed += OnReceiverClosed;
            _receiverLink.Start(1, OnMessage); // Process one message at a time (to maintain sequencing on redelivery)
        }

        void OnReceiverClosed(AmqpObject sender, Error error)
        {
            // AMQP .NET Lite currently has no keepalive option
            if (error != null && error.Description.Contains("connection was forcibly closed"))
            {
                _log.DebugFormat("Connection closed by server - reconnecting");
                Thread.Sleep(500); // don’t hammer it if it is refusing connections temporarily
                Start(_config, _session, _closedCallback, _processor);
            }
            else 
            {
                _closedCallback(sender, error);
            }
        }

        public void Stop()
        {
            if (_receiverLink != null)
                _receiverLink.SetCredit(0);
            else 
                _session.Close();
        }

        public override string ToString()
        {
            return _receiverLink == null ? base.ToString() : _receiverLink.Name;
        }

        class MsgTracker
        {
            int tryCount;
            internal int TryCount { get { return tryCount; } set { tryCount = value; if (tryCount == 1) Sleep = 1000; } }
            internal double Sleep { get; set; }
            internal string MsgId { get; set; }
        }   

        IDictionary<ReceiverLink, MsgTracker> trackers = new Dictionary<ReceiverLink, MsgTracker>();

        void OnMessage(ReceiverLink receiver, Message inMsg)
        {
            NDC.Pop(); NDC.Push(_receiverLink.Name);
            _log.DebugFormat("Received {0} message [{1}] with headers {2}, app properties {3}",
                inMsg.Body.GetType(), inMsg.Body, inMsg.Header, inMsg.ApplicationProperties);

            if (!trackers.ContainsKey(receiver))
            {
                trackers.Add(receiver, new MsgTracker());
            }
            MsgTracker tracker = trackers[receiver];
        
            try
            {
                string newMsgId = (string) inMsg.ApplicationProperties["messageGuid"];
                tracker.TryCount = newMsgId.Equals(tracker.MsgId) ? tracker.TryCount + 1 : 1;
                tracker.MsgId = newMsgId;
                _log.DebugFormat("Attempt {0} to process message id {1}", tracker.TryCount, tracker.MsgId);
                Thread.Yield();
                if (inMsg.Body is string message)
                {
                    // Process message e.g. deserialise to objects and fire some events / persist to a DB
                    _processor.Process(_deadLog, message, inMsg.ApplicationProperties.Map);
                    _receiverLink.Accept(inMsg);
                    _log.DebugFormat("Processed message with id {0}", tracker.MsgId);
					Console.Write(inMsg.Body);
                    tracker.MsgId = null;
                }
                else
                {
                    throw new InvalidCastException(String.Format("Message received of unexpected type {0}", inMsg.Body.GetType()));
                }
            }
            catch (Exception e)
            {
                // Check for exceptions that indicate a problem with the payload/headers (add more as required)
                if (e is InvalidCastException || e is InvalidOperationException || e is NullReferenceException) 
                {
                    Util.LogException(_deadLog, "Missing/malformed message body or properties encountered", e);
                    _deadLog.InfoFormat("Properties: {0}, Payload: {1}", inMsg.ApplicationProperties, inMsg.Body);
                    if (!_receiverLink.IsClosed) 
                        _receiverLink.Reject(inMsg);
                }
                else
                {
                    Util.LogException(_log, "Unhandled exception occurred - will wait and retry", e);
                    if (!_receiverLink.IsClosed)
                    {
                        _receiverLink.Release(inMsg);
                        // Retry with exponential backoff 
                        _log.DebugFormat("Sleeping before redelivery for {0}ms", (int)tracker.Sleep);
                        Thread.Sleep((int)tracker.Sleep);
                        if (tracker.Sleep <= 50000) tracker.Sleep *= 1.75;
                    }
                }
            }        
        }
               
        void OnAttached(Link link, Attach attach)
        {
            NDC.Push(_receiverLink.Name);
            _log.DebugFormat("{0} is attached: {1} {2}", _receiverLink.Name, attach, link);
        }

    }

    class Util
    {
        public static void LogException(ILog log, String message, Exception e)
        {
            log.ErrorFormat("{0}: {1} [{2}]", message, e.GetBaseException().GetType(), e.GetBaseException().Message);
            log.Info(e);
        }
    }

}
