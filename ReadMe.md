Example C# .NET Client for SIS Connect Feed
===========================================

**ALL SAMPLE CODE IS PROVIDED ON AN "AS-IS" BASIS FOR GENERAL GUIDANCE, WITH AN EXISTING UNDERSTANDING OF .NET APIs AND CONVENTIONS BEING PRESUMED. SIS CANNOT PROVIDE GENERAL 
SUPPORT FOR USE OF THE .NET FRAMEWORK OR THE AMQP .NET LITE API, WHICH ARE DOCUMENTED AND DISCUSSED EXTENSIVELY ONLINE. PLEASE SEE https://bitbucket.org/sisbetting/customer_sampleclient 
FOR ADDITIONAL SAMPLES USING OTHER LANGUAGES AND/OR FRAMEWORKS**

This example uses the *AMQPNetLite* open-source NuGet package which is generally referenced by Microsoft in their online AMQP documentation e.g. for integration with Azure Service Bus.

**Prerequisites**

See https://github.com/Azure/amqpnetlite/blob/master/README.md

**Configuration**

To satisfy external package dependencies just open the Nuget Package Manager in Visual Studio to get it to refresh them.

Queue connection parameters are set via App.config. 

Check with your Account Manager or SIS Technical Contact on how to obtain connection credentials and client certificates for each 
environment.

**Consuming the Feed**

Receiver.cs contains a simple client framework, opening a connection and setting up a Receiver Link, and delegating all business 
processing to the IRaceProcessor implementation(s) contained in RaceProcessor.cs:

```java
    public class RaceProcessor: IRaceProcessor
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(RaceProcessor));

        /// <summary>
        /// Method to be called for every incoming message. If a message is "poison" - meaning it could never be processed 
        /// for some reason - e.g. invalid JSON/XML or a required message header is missing, use the deadLetterLog to log 
        /// a suitable error message and return without throwing an exception. For any type of system exception, e.g. 
        /// because a downstream database or queue or web service is currently offline, don't handle it, just throw it and 
        /// the message will be redelivered for retrying after a delay.
        /// </summary>
        /// <param name="deadLetterLog">See summary - configure by log4net.config</param>
        /// <param name="message">string-type message payload</param>
        /// <param name="properties">map of message properties</param>
        public void Process(ILog deadLetterLog, string message, IDictionary properties)
        {
            // Note the message and properties are already logged at DEBUG level

        }

        /// See next example for purpose of this method
        public String getFilter() { return null; }
    }
```

**Splitting the Feed**

Message filters can be applied to divert subsets of messages to two or more event-specific processors, if desired. For example the following processor includes 
a filter on the _categoryCode_ message property, to include only horse racing events:

```java
    public class HorseRaceProcessor: IRaceProcessor
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(HorseRaceProcessor));

        public void Process(ILog deadLetterLog, string message, IDictionary properties)
        {
            // Note the message and properties are already logged at DEBUG level

        }

        /// <summary>
        /// Return an optional message filter - A JMS-style message selector e.g. "categoryCode in ('DG','HR')" - or null if not required
        /// </summary>
        public String getFilter() {
            return "categoryCode = 'HR'";
        }
    }
```
See the SIS Connect Wiki for available message properties that can be used to split feed processing: https://siswiki.sis.tv:8090/display/SC/High+Level+Overview

**Error Handling**

Basic error handling is included in the sample code, for:

- "Poison messages" - messages that are invalid and could therefore never be processed - which are diverted to a Dead Letter Log (endpoint 
configurable via log4net.config). While poison messages should never be encountered in production they need to be accommodated as a possibility, to prevent the queue becoming blocked
 
- Unhandled exceptions, resulting in redelivery (assuming that there is some downstream problem with e.g. a web service or a database), logging an error - i.e. to be picked up by an external monitor - and delaying redelivery with exponential backoff
