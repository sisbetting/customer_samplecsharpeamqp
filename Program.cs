﻿using log4net;
using log4net.Config;
using System;
using System.Configuration;
using System.IO;
using System.Threading;

namespace tv.sis.connect
{
    class BootStrap
    {
        static readonly ManualResetEvent _quitEvent = new ManualResetEvent(false);
        static readonly ILog _log = LogManager.GetLogger(typeof(BootStrap));

        static void Main(string[] args)
        {
            string logCfg = ConfigurationManager.AppSettings["log4netCfg"];
            XmlConfigurator.Configure(new FileInfo(logCfg));
            log4net.NDC.Push("main");

            Console.CancelKeyPress += delegate (object sender, ConsoleCancelEventArgs e) {
                e.Cancel = true;
                _quitEvent.Set();
            };

            AmqpClient client = new AmqpClient();
            client.Start(_quitEvent);

            _quitEvent.WaitOne();
            _log.Info("Interrupted - Stopping...");
            client.Stop();

            if (args.Length > 0 && args[0].Equals("--interactive"))
            {
                Console.Write("Hit <Enter> to exit...");
                Console.ReadLine();
            }
        }
    }

}
