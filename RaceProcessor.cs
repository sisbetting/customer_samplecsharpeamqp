﻿using log4net;
using System;
using System.Collections;

namespace tv.sis.connect
{
    /// <summary>
    /// Simple IoC configuration 
    /// </summary>
    public static class ProcessorConfig
    {
        /// <summary>
        /// List of processors to start receivers for automatically
        /// </summary>
        public static readonly IRaceProcessor[] processors = {
            new DogRaceProcessor(),
            new HorseRaceProcessor()
        };
    }

    /// <summary>
    /// Example race feed message processor that processes only events related to dog racing
    /// </summary>
    public class DogRaceProcessor : IRaceProcessor
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(HorseRaceProcessor));

        // See comment on IRaceProcessor.Process() re error handling etc.
        public void Process(ILog deadLetterLog, string message, IDictionary properties)
        {
            // Note the message and properties are already logged at DEBUG level
            
        }

        // See comment on IRaceProcessor.GetFilter() for usage/purpose
        public string GetFilter()
        {
            return "categoryCode = 'DG'";
        }
    }

    /// <summary>
    /// Example race feed message processor that processes only events related to horse racing
    /// </summary>
    public class HorseRaceProcessor : IRaceProcessor
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(HorseRaceProcessor));

        // See comment on IRaceProcessor.Process() re error handling etc.
        public void Process(ILog deadLetterLog, string message, IDictionary properties)
        {
            // Note the message and properties are already logged at DEBUG level
            
        }

        // See comment on IRaceProcessor.GetFilter() for usage/purpose
        public string GetFilter()
        {
            return "categoryCode = 'HR'";
        }
    }

    public interface IRaceProcessor
    {
        /// <summary>
        /// Method to be called for every incoming message. If a message is "poison" - meaning it could never be processed 
        /// for some reason - e.g. invalid JSON/XML or a required message header is missing, use the deadLetterLog to log 
        /// a suitable error message and return without throwing an exception. For any type of system exception, e.g. 
        /// because a downstream database or queue or web service is currently offline, don't handle it, just throw it and 
        /// the message will be redelivered for retrying after a delay.
        /// </summary>
        /// <param name="deadLetterLog">See summary - configure by log4net.config</param>
        /// <param name="message">string-type message payload</param>
        /// <param name="properties">map of message properties</param>
        void Process(ILog deadLetterLog, string message, IDictionary properties);

        /// <summary>
        /// Return an optional message filter - A JMS-style message selector e.g. "categoryCode in ('DG','HR')" - or null if not required
        /// </summary>
        string GetFilter();
    }

}